variable "vsphere_user" {
  type = string
}

variable "vsphere_password" {
  type = string
}

variable "vsphere_server" {
  type = string
}

variable "datacenter" {
  type = string
}

variable "datastore_1" {
  type = string
}

variable "datastore_2" {
  type = string
}

variable "pool" {
  type = string
}

variable "network" {
  type = string
}

variable "template" {
  type = string
}

variable "folder" {
  type = string
}

variable "dns_servers" {
  type = list(string)
}

variable "admin_password" {
  type = string
}

variable "ssh_pub_key" {
  type = string
}

variable "dns_domain" {
  type = string
}

variable "machines" {
  type = map(object({
    hostname = string
    domain = string
    ipv4_address = string
    ipv4_netmask = number
    ipv4_gateway = string
    num_cpus = number
    num_cores_per_socket = number
    memory = number
    disks = list(object({
      label = string
      size = number
      unit_number = number
    }))
  }))
}
