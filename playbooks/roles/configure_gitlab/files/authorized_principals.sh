#!/bin/bash

KEY_ID=$1
CMD="command=\"/opt/gitlab/embedded/service/gitlab-shell/bin/gitlab-shell"
PERMS="no-port-forwarding,no-X11-forwarding,no-agent-forwarding,no-pty"
GITLAB_USERNAME="username-$KEY_ID\""
LOG_FILE="/etc/gitlab/sshca/sshca.log"
DOMAINS=(
  *.rmt
  *.redmoose.tech
  *.redmoosetech.com
  172.16.0.0/24
  172.16.10.0/24
  192.168.4.0/24
)
PRINCIPALS=(
  administrator
  developer
)
EMAILS=(
  "@redmoose.tech"
  "@redmoosetech.com"
)

AUTH_PRINC="${PRINCIPALS[*]}"
AUTH_DOM="from=\"$(IFS=,; echo "${DOMAINS[*]}")\""


function log() {
  echo "$@" >> $LOG_FILE
}

function fail() {
  log "$@"
  exit 1
}

# Normalize KeyId to Name if email
for email in "${EMAILS[@]}"; do
  if [[ $KEY_ID =~ $email ]]; then
    GITLAB_USERNAME="username-$(echo "$KEY_ID"|cut -d'@' -f1)\""
  fi
done

echo "$AUTH_DOM $CMD $GITLAB_USERNAME,$PERMS $AUTH_PRINC"